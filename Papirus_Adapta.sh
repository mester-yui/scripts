#!/bin/bash

echo 1º script realizado por Juan Febles - Podcast Linux 
echo Instalación de Icono Papirus, Tema Adpata Nokto e iconos de sistema en blanco.

sleep 1

sudo add-apt-repository ppa:papirus/papirus
sudo apt-get update
sudo apt-get install papirus-icon-theme
sudo apt-get install libreoffice-style-papirus

sleep 1

sudo add-apt-repository ppa:andreas-angerer89/sni-qt-patched
sudo apt update
sudo apt install sni-qt sni-qt:i386 hardcode-tray
sudo -E hardcode-tray --conversion-tool RSVGConvert --size 22 --theme Papirus

sleep 1

sudo apt-add-repository ppa:tista/adapta
sudo apt-get update
sudo apt-get install adapta-gtk-theme

exit
